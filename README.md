# Gemicle test be app

## Before a start

You need to have FE-part for this repo. You can take it on next link [https://bitbucket.org/volos_86/gemicle-test-fe/src/master/](https://bitbucket.org/volos_86/gemicle-test-fe/src/master/)

## Install and run

```
npm i
npm start
```
