const WebSocket = require('ws');
const fs = require('fs');
const readline = require('readline');

const data = require('./assets/111.json');

const wss = new WebSocket.Server({ port: 6969 });

wss.on('connection', function connection(ws) {
    ws.on('message', function incoming(responce) {
        console.log('received: %s', responce);
        let msg = JSON.parse(responce);
        if(msg.message === 'build') {
            processLineByLine(ws)
        } else {
            ws.send(JSON.stringify({author: 'SERVER', message: 'Bad command!!!', type: 'log'}));
        }
    });

});

async function processLineByLine(ws) {
    startBuild(ws);

    for await (const step of data.steps) {
        let startMessage = JSON.stringify({
            creationTimeStamp: step.creationTimeStamp,
            name: step.name,
            status: 'inProccess'
        })
        ws.send(JSON.stringify({author: 'SERVER', type: 'step', message: startMessage}));
        await sleep(Math.random() * 100)
        for await(const log of step.logs) {
            ws.send(JSON.stringify({author: 'SERVER', type: 'log', message: log}));
            await sleep(Math.random() * 100)
        }
        await sleep(Math.random() * 100)

        let finishMessage = JSON.stringify({
            name: step.name,
            finishTimeStamp: step.finishTimeStamp,
            status: step.status
        })
        ws.send(JSON.stringify({author: 'SERVER', type: 'step', message: finishMessage}));
    }


}

function startBuild(ws) {
    const welcomeMsg = JSON.stringify(data.steps.map(item => item.name))
    ws.send(JSON.stringify({author: 'SERVER', type: 'startMsg', message: welcomeMsg}));
}

function sleep(ms){
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}
